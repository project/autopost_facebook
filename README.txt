CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Autopost Facebook module allows the user to automatically post entities on
Facebook using a single site's global Facebook account.

 * For a full description of the module visit:
   https://www.drupal.org/project/autopost_facebook

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/autopost_facebook


REQUIREMENTS
------------

This module requires the following outside of Drupal core.

 * Create a Facebook App following Facebook developers documentation:
   https://developers.facebook.com/docs/apps/register/


INSTALLATION
------------

 * Install the Autopost Facebook module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.

Please note: You have to use composer to install the module! Otherwise the
dependencies won't be resolved and the autoloader will not be configured
correctly.


CONFIGURATION
-------------

    1. Create a Facebook App following Facebook developers documentation.
    2. Navigate to Administration > Extend and enable the module.
    3. Navigate to Administration > Configuration > Web Services > Autopost
       Facebook and set the App ID and secret for the App created by Facebook
       App.
    4. Navigate to Administration > Configuration > Web Services > Autopost
       Facebook > Account to configure the Account where entities will be
       published. Select the "Add account" button, log in to the account
       and authorize the App.
    5. Add a new field of type "Autopost Facebook" to an entity (e.g. node
       page).
    6. When creating/updating an entity, the autoposting behavior is defined by
       the value of the "Autopost Facebook" field. There are 3 possible values:
       Don't post, Post only once, or Post on every update. Save.


MAINTAINERS
-----------

 * piggito - https://www.drupal.org/u/piggito

Supporting organization :

 * Skilld - https://www.drupal.org/skilld
